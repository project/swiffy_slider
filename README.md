# Swiffy slider

Drupal integration of <a href="https://github.com/dynamicweb/swiffy-slider">Swiffy Slider</a>.
This module provides a field formatter for the field type <code>entity_reference</code> and a views format.
Swiffy Slider provides a lightweight slider/carousel solution. All the sliding, dragging, snapping etc.
are native browser behavior and not Javascript. Swiffy Slider can even run in a simple mode with no JS at all.
Support for any input device (including touch pads, pencils, assistive devices) and 100% WCAG compatible.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/swiffy_slider).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/swiffy_slider).


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements


This module ships already the required library, but it is possible to override the used version.

1. Add the following snippet to your composer.json inside repositories


```json lines
{
  "type": "package",
  "package": {
    "type": "drupal-library",
    "name": "dynamicweb/swiffy-slider",
    "version": "v1.6.0",
    "dist": {
      "url": "https://github.com/dynamicweb/swiffy-slider/archive/refs/tags/v1.6.0.zip",
      "type": "zip"
    }
  }
}
```

2. Download the dependency

```shell
composer require dynamicweb/swiffy-slider
```


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to https://swiffyslider.com/configuration/ and customize the slider options, copy the **Perma link**

2. Select the formatter **Swiffy Slider** for a entity reference field or views format **Swiffy slider**

3. Click settings (the gear icon) and paste the copied **Perma link**
