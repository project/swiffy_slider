<?php

/**
 * @file
 * Post update functions for Swiffy Slider.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\views\ViewEntityInterface;

/**
 * Updates swiffy_slider.settings:configuration_url config if it's still at the default.
 */
function swiffy_slider_post_update_set_configuration_url_to_null_1(): void {
  $swiffy_slider_settings = \Drupal::configFactory()->getEditable('swiffy_slider.settings');
  if (!$swiffy_slider_settings->isNew() && $swiffy_slider_settings->get('configuration_url') === '') {
    $swiffy_slider_settings
      ->set('configuration_url', NULL)
      ->save(TRUE);
  }
}

/**
 * Set the configuration url in swiffy_slider view style to null for empty urls.
 */
function swiffy_slider_post_update_set_configuration_url_to_null_2(array &$sandbox): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'view', function (ViewEntityInterface $view): bool {
    $displays = $view->get('display');
    if (!is_array($displays)) {
      return FALSE;
    }

    $update = FALSE;
    foreach ($displays as &$display) {
      if (!is_array($display)) {
        continue;
      }
      $display_options = $display['display_options'] ?? NULL;
      if (!is_array($display_options)) {
        continue;
      }
      $style = $display_options['style'] ?? NULL;
      if (!is_array($style)) {
        continue;
      }
      $style_type = $style['type'] ?? NULL;
      if (!is_string($style_type)) {
        continue;
      }
      /** @var array{"display_options": array{"style": array{"type": string, "options" : array{"configuration_url": ?string}}}} $display */
      if ($style_type === 'swiffy_slider' && $display['display_options']['style']['options']['configuration_url'] === '') {
        $display['display_options']['style']['options']['configuration_url'] = NULL;
        $update = TRUE;
      }
    }

    unset($display);
    if ($update) {
      $view->set('display', $displays);
    }

    return $update;
  });
}

/**
 * Set the configuration url in swiffy_slider field formatter to null for empty urls.
 */
function swiffy_slider_post_update_set_configuration_url_to_null_3(array &$sandbox): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'entity_view_display', function (EntityViewDisplayInterface $entityViewDisplay): bool {
    $update = FALSE;
    /** @var array<array[]> $components */
    $components = $entityViewDisplay->getComponents();
    foreach ($components as $name => $component) {
      if (!isset($component['settings']['swiffy_slider_permalink'])) {
          continue;
      }

      if (!is_string($name)) {
          continue;
      }

      if ($component['settings']['swiffy_slider_permalink'] === '') {
        $component['settings']['swiffy_slider_permalink'] = NULL;
        $entityViewDisplay->setComponent($name, $component);
        $update = TRUE;
      }
    }

    return $update;
  });
}
