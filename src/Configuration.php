<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use function array_filter;
use function array_merge;
use function array_values;
use function is_null;
use function is_string;

/**
 * Supplies render arrays for Swiffy options to be used in configuration forms.
 *
 * @internal
 */
final class Configuration implements ConfigurationInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration object factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationUrlElement(?string $defaultValue = NULL): array {
    return [
      '#type' => 'url',
      '#title' => $this->t('Configuration URL'),
      '#description' => $this->t('Copy and paste the URL from @target configuration page.', [
        '@target' => Link::fromTextAndUrl('Swiffy slider', Url::fromUri(self::CONFIGURATOR_URL))->toString(),
      ]),
      '#maxlength' => 1000,
      '#default_value' => $defaultValue,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toAttributes(?string $configurationURL = NULL): Attribute {
    if (is_null($configurationURL)) {
      $defaultConfigurationURL = $this->configFactory->get('swiffy_slider.settings')->get('configuration_url');
      if (is_string($defaultConfigurationURL)) {
        $configurationURL = $defaultConfigurationURL;
      }
    }

    if (!is_string($configurationURL)) {
      $configurationURL = self::CONFIGURATOR_URL;
    }

    /** @var array{path: ?string,query: array,fragment:string} $parsed_url */
    $parsed_url = UrlHelper::parse($configurationURL);
    $attributes = new Attribute();
    $classes = array_values(array_filter($parsed_url['query'], static fn(string $key): bool => str_starts_with($key, 'slider-'), ARRAY_FILTER_USE_KEY));
    $attributes['class'] = array_merge($classes, ['swiffy-slider']);
    $data_attributes = array_filter($parsed_url['query'], static fn(string $key): bool => str_starts_with($key, 'data'), ARRAY_FILTER_USE_KEY);
    $style = (string) (new Attribute(array_filter($parsed_url['query'], static fn(string $key): bool => str_starts_with($key, '--swiffy-slider'), ARRAY_FILTER_USE_KEY)));
    if ($style !== '') {
      $attributes['style'] = $style;
    }
    $attributes = array_merge($attributes->toArray(), $data_attributes);
    return new Attribute($attributes);
  }

}
