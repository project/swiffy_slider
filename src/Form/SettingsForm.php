<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\swiffy_slider\ConfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function is_string;

/**
 * Configuration form for Swiffy Slider.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * Internal configuration helper.
   *
   * @var \Drupal\swiffy_slider\ConfigurationInterface
   */
  protected ConfigurationInterface $configuration;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SettingsForm {
    /** @var \Drupal\swiffy_slider\Form\SettingsForm $instance */
    $instance = parent::create($container);
    $instance->configuration = $container->get('swiffy_slider.configuration');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'swiffy_slider_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['swiffy_slider.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $url = $this->config('swiffy_slider.settings')->get('configuration_url');
    $element = $this->configuration->configurationUrlElement(is_string($url) ? $url : NULL);
    $element['#description'] .= ' ' . $this->t('This configuration is used as default configuration.');
    $form['configuration_url'] = $element;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('swiffy_slider.settings');
    $url = $form_state->getValue('configuration_url');
    if ($url !== '') {
      $config->set('configuration_url', $url);
    }
    else {
      $config->set('configuration_url', NULL);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
