<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider;

use Drupal\Core\Template\Attribute;

/**
 * Supplies render arrays for Swiffy options to be used in configuration forms.
 */
interface ConfigurationInterface {

  /**
   * The URL to the swiffy slider configurator.
   */
  public const CONFIGURATOR_URL = 'https://swiffyslider.com/configuration/';

  /**
   * Generates a render array for the swiffy configurator URL.
   *
   * @param string|null $defaultValue
   *   The actual value.
   *
   * @return array{
   *   "#type": string,
   *   "#title": \Drupal\Core\StringTranslation\TranslatableMarkup,
   *   "#description": \Drupal\Core\StringTranslation\TranslatableMarkup,
   *   "#maxlength": 1000,
   *   "#default_value": string|null
   *   }
   *   An url field as render array.
   */
  public function configurationUrlElement(?string $defaultValue = NULL): array;

  /**
   * Converts the swiffy configurator URL into an attributes array.
   *
   * @param string|null $configurationURL
   *   Url from the swiffy configurator.
   *
   * @return \Drupal\Core\Template\Attribute
   *   Attributes extracted from url.
   */
  public function toAttributes(?string $configurationURL = NULL): Attribute;

}
