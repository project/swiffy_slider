<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\swiffy_slider\ConfigurationInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function array_key_exists;
use function is_string;

/**
 * Swiffy Slider style plugin.
 *
 * @ViewsStyle(
 *   id = "swiffy_slider",
 *   title = @Translation("Swiffy Slider"),
 *   help = @Translation("Display rows as swiffy slider."),
 *   theme = "views_style_swiffy_slider",
 *   display_types = {"normal"}
 * )
 */
final class SwiffySlider extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * Storage for available option given by the library.
   *
   * @var \Drupal\swiffy_slider\ConfigurationInterface
   */
  protected ConfigurationInterface $swiffyOptions;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition,);
    $instance->swiffyOptions = $container->get('swiffy_slider.configuration');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    return ['configuration_url' => ['default' => NULL]];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(mixed &$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $current_value = NULL;
    if (array_key_exists('configuration_url', $this->options) && is_string($this->options['configuration_url'])) {
      $current_value = $this->options['configuration_url'];
    }
    /** @var array $form */
    $form['configuration_url'] = $this->swiffyOptions->configurationUrlElement($current_value);
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state): void {
    if ($form_state->getValue(['style_options', 'configuration_url']) === '') {
      $form_state->setValue(['style_options', 'configuration_url'], NULL);
    }
  }

}
