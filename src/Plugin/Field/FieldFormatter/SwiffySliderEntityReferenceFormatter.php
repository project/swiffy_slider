<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'Swiffy Slider' formatter.
 *
 * @FieldFormatter(
 *   id = "swiffy_slider_entity_reference",
 *   label = @Translation("Swiffy Slider"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class SwiffySliderEntityReferenceFormatter extends EntityReferenceEntityFormatter {

  use SwiffySliderFieldFormatterTrait;

}
