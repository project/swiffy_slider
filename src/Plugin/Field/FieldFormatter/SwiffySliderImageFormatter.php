<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Swiffy Slider' formatter.
 *
 * @FieldFormatter(
 *   id = "swiffy_slider_image",
 *   label = @Translation("Swiffy Slider"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class SwiffySliderImageFormatter extends ImageFormatter {

  use SwiffySliderFieldFormatterTrait;

}
