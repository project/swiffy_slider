<?php

declare(strict_types=1);

namespace Drupal\swiffy_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\swiffy_slider\ConfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function array_key_exists;
use function is_array;
use function is_string;
use function sprintf;
use function trim;

/**
 * Trait for field formatter.
 */
trait SwiffySliderFieldFormatterTrait {

  /**
   * Storage for available option given by the library.
   *
   * @var \Drupal\swiffy_slider\ConfigurationInterface
   */
  protected ConfigurationInterface $swiffyOptions;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->swiffyOptions = $container->get('swiffy_slider.configuration');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $settings = parent::defaultSettings();
    unset($settings['link']);
    $settings['swiffy_slider_permalink'] = NULL;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);
    $elements['swiffy_slider_permalink'] = $this->swiffyOptions->configurationUrlElement($this->getConfiguredUrl());
    $elements['swiffy_slider_permalink']['#element_validate'] = [[$this, 'emptyConfigurationUrlToNull']];
    return $elements;
  }

  /**
   * Set the form value to null, when the submitted value is an empty string.
   */
  public static function emptyConfigurationUrlToNull(array $element, FormStateInterface $form_state): void {
    $element_value = $element['#value'] ?? NULL;
    if (is_string($element_value)) {
      $value = trim($element_value);
      if ($value === '') {
        $form_state->setValueForElement($element, NULL);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $uri = $this->getConfiguredUrl();
    $text = $this->t('Customized');
    if ($uri === NULL) {
      $text = $this->t('Default');
      $uri = ConfigurationInterface::CONFIGURATOR_URL;
    }

    $summary[] = Markup::create(sprintf('%s: %s', $this->t('Swiffy slider configuration'), Link::fromTextAndUrl($text, Url::fromUri($uri))->toString()));
    // phpcs:disable
    /** @phpstan-var string[] $summary */
    // phpcs:enable
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL): array {
    $elements = parent::view($items, $langcode);
    if ($items->isEmpty()) {
      return $elements;
    }

    if (!array_key_exists('#attached', $elements) || !is_array($elements['#attached'])) {
      $elements['#attached'] = [];
    }
    if (!array_key_exists('library', $elements['#attached']) || !is_array($elements['#attached']['library'])) {
      $elements['#attached']['library'] = [];
    }
    $elements['#attached']['library'][] = 'swiffy_slider/swiffy_slider-lib';
    $url = $this->getConfiguredUrl();
    $elements['#swiffy_slider_attributes'] = $this->swiffyOptions->toAttributes($url);
    if ($url === NULL) {
      if (!array_key_exists('#cache', $elements) || !is_array($elements['#cache'])) {
        $elements['#cache'] = [];
      }
      if (!array_key_exists('tags', $elements['#cache']) || !is_array($elements['#cache']['tags'])) {
        $elements['#cache']['tags'] = [];
      }
      $elements['#cache']['tags'][] = 'config:swiffy_slider.settings';
    }

    return $elements;
  }

  /**
   * Returns the configured URL of the formatter settings or null.
   */
  protected function getConfiguredUrl(): ?string {
    $uri = $this->getSetting('swiffy_slider_permalink');
    if (is_string($uri) && $uri !== '') {
      return $uri;
    }

    return NULL;
  }

}
