<?php

declare(strict_types=1);

namespace Drupal\Tests\swiffy_slider\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\swiffy_slider\Configuration;

/**
 * @coversDefaultClass \Drupal\swiffy_slider\Configuration
 * @group swiffy_slider
 */
class ConfigurationTest extends UnitTestCase {

  /**
   * @covers ::toAttributes
   * @dataProvider toAttributesData
   */
  public function testToAttributes(?string $configuration_url, array $expected_attributes_array, string $expected_attributes_string, ?string $default_configuration_url = NULL): void {
    $configFactory = $this->getConfigFactoryStub(['swiffy_slider.settings' => ['configuration_url' => $default_configuration_url]]);
    $option = (new Configuration($configFactory));
    $attributes = $option->toAttributes($configuration_url);
    self::assertEquals($expected_attributes_array, $attributes->toArray());
    self::assertEquals($expected_attributes_string, (string) $attributes);
  }

  /**
   * Data provider for ::testToAttributes.
   *
   * @see testToAttributes
   */
  public static function toAttributesData(): iterable {
    // phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
    yield [
      'https://swiffyslider.com/configuration/?slider-item-show=slider-item-show2&data-slider-nav-autoplay-interval=1000&--swiffy-slider-nav-light=%23000',
      ['class' => ['slider-item-show2', 'swiffy-slider'], 'data-slider-nav-autoplay-interval' => '1000', 'style' => ' --swiffy-slider-nav-light="#000"'],
      ' class="slider-item-show2 swiffy-slider" style=" --swiffy-slider-nav-light=&quot;#000&quot;" data-slider-nav-autoplay-interval="1000"',
    ];
    yield [
      'https://swiffyslider.com/configuration/?slider-item-show=slider-item-show2&data-slider-nav-autoplay-interval=1000',
      ['class' => ['slider-item-show2', 'swiffy-slider'], 'data-slider-nav-autoplay-interval' => '1000'],
      ' class="slider-item-show2 swiffy-slider" data-slider-nav-autoplay-interval="1000"',
    ];
    yield [
      'https://swiffyslider.com/configuration/?slider-item-show=slider-item-show2',
      ['class' => ['slider-item-show2', 'swiffy-slider']],
      ' class="slider-item-show2 swiffy-slider"',
    ];
    yield [
      'https://swiffyslider.com/configuration/',
      ['class' => ['swiffy-slider']],
      ' class="swiffy-slider"',
    ];
    yield [
      NULL,
      ['class' => ['swiffy-slider']],
      ' class="swiffy-slider"',
    ];
    yield [
      NULL,
      ['class' => ['slider-item-show2', 'swiffy-slider'], 'data-slider-nav-autoplay-interval' => '1000', 'style' => ' --swiffy-slider-nav-light="#000"'],
      ' class="slider-item-show2 swiffy-slider" style=" --swiffy-slider-nav-light=&quot;#000&quot;" data-slider-nav-autoplay-interval="1000"',
      'https://swiffyslider.com/configuration/?slider-item-show=slider-item-show2&data-slider-nav-autoplay-interval=1000&--swiffy-slider-nav-light=%23000',
    ];
    // phpcs:enable Drupal.Arrays.Array.LongLineDeclaration
  }

}
