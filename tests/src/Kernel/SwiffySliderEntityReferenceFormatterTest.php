<?php

declare(strict_types=1);

namespace Drupal\Tests\swiffy_slider\Kernel;

/**
 * Tests the swiffy slider formatter functionality.
 *
 * @group swiffy_slider
 */
class SwiffySliderEntityReferenceFormatterTest extends SwiffySliderKernelTestBase {

  /**
   * Test html output of field formatter.
   */
  public function testFieldFormatter(): void {
    $node = $this->createDummyContent();
    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $build = $view_builder->view($node, 'default');
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->setRawContent((string) $output);
    $this->assertRaw('<div class="swiffy-slider">');
    $this->assertRaw('<ul class="slider-container">');
    $this->assertRaw('<span>Item 1</span>');
    $this->assertRaw('<span>Item 2</span>');
    $this->assertRaw('<button type="button" class="slider-nav" aria-label="Go to previous"></button>');
    $this->assertRaw('<button type="button" class="slider-nav slider-nav-next" aria-label="Go to next"></button>');
    $this->assertRaw('<button class="active"  aria-label="Go to slide"></button>');
    $this->assertRaw('<button  aria-label="Go to slide"></button>');
    $this->assertNoRaw('<ul class="links inline"><li><a href="/node/1" rel="tag" title="Item 1" hreflang="en">Read more<span class="visually-hidden"> about Item 1</span></a></li></ul>');
    $this->assertNoRaw('<ul class="links inline"><li><a href="/node/2" rel="tag" title="Item 2" hreflang="en">Read more<span class="visually-hidden"> about Item 2</span></a></li></ul>');

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $repository */
    $repository = $this->container->get('entity_display.repository');
    $display = $repository->getViewDisplay('node', 'swiffy_slider_test');
    /** @var array{"settings": array} $component */
    $component = $display->getComponent('field_ref_item');
    $component['settings']['view_mode'] = 'teaser';
    $component['settings']['swiffy_slider_permalink'] = 'https://example.org/configuration/?slider-item-show=slider-item-show2&data-slider-nav-autoplay-interval=1000&--swiffy-slider-nav-light=%23000';
    $display->setComponent('field_ref_item', $component)->save();

    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $build = $view_builder->view($node, 'default');
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->setRawContent((string) $output);
    $this->assertRaw('<div class="slider-item-show2 swiffy-slider" style=" --swiffy-slider-nav-light=&quot;#000&quot;" data-slider-nav-autoplay-interval="1000">');
    $this->assertRaw('<ul class="links inline"><li><a href="/node/1" rel="tag" title="Item 1" hreflang="en">Read more<span class="visually-hidden"> about Item 1</span></a></li></ul>');
    $this->assertRaw('<ul class="links inline"><li><a href="/node/2" rel="tag" title="Item 2" hreflang="en">Read more<span class="visually-hidden"> about Item 2</span></a></li></ul>');
  }

}
