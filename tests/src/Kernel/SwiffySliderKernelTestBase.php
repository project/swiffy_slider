<?php

namespace Drupal\Tests\swiffy_slider\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Reusable base class for tests.
 */
abstract class SwiffySliderKernelTestBase extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'swiffy_slider',
    'swiffy_slider_content_type',
    'node',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::service('theme_installer')->install(['stark']);
    $this->config('system.theme')->set('default', 'stark')->save();
    $this->installConfig([
      'system',
      'node',
      'user',
      'swiffy_slider_content_type',
    ]);
    $role = Role::load(RoleInterface::ANONYMOUS_ID);
    if ($role instanceof RoleInterface) {
      $role
        ->grantPermission('access content')
        ->save();
    }
  }

  /**
   * Used by tests.
   */
  protected function createDummyContent(): NodeInterface {
    $item_ref_1 = Node::create(['type' => 'swiffy_slider_ref_item']);
    $item_ref_1->setTitle('Item 1');
    $item_ref_1->save();

    $item_ref_2 = Node::create(['type' => 'swiffy_slider_ref_item']);
    $item_ref_2->setTitle('Item 2');
    $item_ref_2->save();

    $node = Node::create(['type' => 'swiffy_slider_test']);
    $node->setTitle('test');

    $value = [];
    $value[] = ['target_id' => $item_ref_1->id()];
    $value[] = ['target_id' => $item_ref_2->id()];
    $node->set('field_ref_item', $value);

    $node->save();
    return $node;
  }

}
