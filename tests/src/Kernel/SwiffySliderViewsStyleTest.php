<?php

declare(strict_types=1);

namespace Drupal\Tests\swiffy_slider\Kernel;

use Drupal\views\Views;

/**
 * Tests the swiffy slider ViewsStyle plugin functionality.
 *
 * @group swiffy_slider
 */
class SwiffySliderViewsStyleTest extends SwiffySliderKernelTestBase {

  /**
   * Test html output of views style plugin.
   */
  public function testViewsStylePlugin(): void {
    $this->createDummyContent();

    $view = Views::getView('swiffy_slider_view');
    self::assertNotNull($view);
    /** @var \Drupal\views\ViewExecutable $view */
    /** @var array $build */
    $build = $view->render();
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->setRawContent((string) $output);
    // Global configuration url is not set and not in the view.
    $this->assertRaw('<div class="swiffy-slider">');
    $this->assertRaw('<ul class="slider-container">');
    $this->assertRaw('<button type="button" class="slider-nav" aria-label="Go to previous"></button>');
    $this->assertRaw('<button type="button" class="slider-nav slider-nav-next" aria-label="Go to next"></button>');
    $this->assertRaw('<button class="active"  aria-label="Go to slide"></button>');
    $this->assertRaw('<div class="slider-indicators">');
    $this->assertRaw('<button  aria-label="Go to slide"></button>');

    // Global configuration url is not set, but in the view.
    $view->style_plugin->options['configuration_url'] = 'https://example.org/configuration/?slider-item-ratio-value=slider-item-ratio-1x1&slider-nav-dark=slider-nav-dark';
    /** @var array $build */
    $build = $view->render();
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->setRawContent((string) $output);
    $this->assertRaw('<div class="slider-item-ratio-1x1 slider-nav-dark swiffy-slider">');

    // Global configuration url is set, but not in the view.
    $this->container
      ->get('config.factory')
      ->getEditable('swiffy_slider.settings')
      ->set('configuration_url', 'https://example.org/configuration/?slider-item-show=slider-item-show2&data-slider-nav-autoplay-interval=1000&--swiffy-slider-nav-light=%23000')
      ->save();
    /** @var \Drupal\Core\DrupalKernelInterface $kernel */
    $kernel = \Drupal::service('kernel');
    $kernel->rebuildContainer();
    $view->style_plugin->options['configuration_url'] = NULL;
    /** @var array $build */
    $build = $view->render();
    $output = $this->container->get('renderer')->renderRoot($build);
    $this->setRawContent((string) $output);
    $this->assertRaw('<div class="slider-item-show2 swiffy-slider" style=" --swiffy-slider-nav-light=&quot;#000&quot;" data-slider-nav-autoplay-interval="1000">');
  }

}
