<?php

namespace Drupal\Tests\swiffy_slider\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;
use Drupal\Tests\UpdatePathTestTrait;
use Drupal\views\Entity\View;
use Drupal\views\ViewEntityInterface;
use function assert;
use function is_array;

/**
 * Tests update of configuration_url if it's still the default of "".
 *
 * @group swiffy_slider
 * @covers \swiffy_slider_post_update_set_configuration_url_to_null_1
 * @covers \swiffy_slider_post_update_set_configuration_url_to_null_2
 * @covers \swiffy_slider_post_update_set_configuration_url_to_null_3
 */
class SwiffySliderDefaultConfigurationURLUpdateTest extends UpdatePathTestBase {

  use UpdatePathTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles(): void {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-10.3.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/swiffy_slider.php',
    ];
  }

  /**
   * Tests update of configuration_url, swiffy_slider_permalink.
   */
  public function testUpdate(): void {
    $configuration_url_before = $this->config('swiffy_slider.settings')->get('configuration_url');
    self::assertSame('', $configuration_url_before);

    $view = View::load('empty_configuration_url');
    assert($view instanceof ViewEntityInterface);
    $data = $view->toArray();
    // @phpstan-ignore-next-line
    $configuration_url = $data['display']['default']['display_options']['style']['options']['configuration_url'];
    self::assertSame('', $configuration_url);

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display */
    $entity_display = \Drupal::service('entity_display.repository');
    $view_display = $entity_display->getViewDisplay('node', 'article', 'default');
    $component = $view_display->getComponent('field_tags');
    assert(is_array($component));
    self::assertSame(['view_mode' => 'default', 'swiffy_slider_permalink' => ''], $component['settings']);
    $this->runUpdates();

    $configuration_url_after = $this->config('swiffy_slider.settings')->get('configuration_url');
    self::assertNull($configuration_url_after);
    $view = View::load('empty_configuration_url');
    assert($view instanceof ViewEntityInterface);
    $data = $view->toArray();
    // @phpstan-ignore-next-line
    $configuration_url = $data['display']['default']['display_options']['style']['options']['configuration_url'];
    self::assertNull($configuration_url);

    $view_display = $entity_display->getViewDisplay('node', 'article', 'default');
    $component = $view_display->getComponent('field_tags');
    assert(is_array($component));
    self::assertSame(['view_mode' => 'default', 'swiffy_slider_permalink' => NULL], $component['settings']);
  }

}
