<?php

declare(strict_types=1);

namespace Drupal\Tests\swiffy_slider\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Testing the swiffy_slider settings.
 *
 * @group swiffy_slider
 */
class SwiffySliderSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'swiffy_slider',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->createUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($account);
  }

  /**
   * Tests that the swiffy_slider settings form stores a `null` Configuration URL.
   */
  public function testSettingsForm(): void {
    $assert_session = $this->assertSession();

    self::assertNull($this->config('swiffy_slider.settings')->get('configuration_url'));
    $this->drupalGet(Url::fromRoute('swiffy_slider.settings'));
    $assert_session->fieldExists('configuration_url');

    $this->submitForm([
      'configuration_url' => '',
    ], 'Save configuration');
    $assert_session->statusMessageContains('The configuration options have been saved.', 'status');
    self::assertNull($this->config('swiffy_slider.settings')->get('configuration_url'));
  }

}
