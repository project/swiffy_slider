<?php
// phpcs:ignoreFile

use Drupal\Core\Database\Database;
use Drupal\Core\Serialization\Yaml;

$connection = Database::getConnection();

// Set the schema version.
$connection->merge('key_value')
  ->fields([
    'value' => 'i:8000;',
    'name' => 'swiffy_slider',
    'collection' => 'system.schema',
  ])
  ->condition('collection', 'system.schema')
  ->condition('name', 'swiffy_slider')
  ->execute();

// Update core.extension.
$extensions = $connection->select('config')
  ->fields('config', ['data'])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute()
  ->fetchField();
$extensions = unserialize($extensions, ['allowed_classes' => FALSE]);
$extensions['module']['swiffy_slider'] = 0;
$connection->update('config')
  ->fields(['data' => serialize($extensions)])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute();


// Create swiffy_slider.settings.
$connection->insert('config')
  ->fields([
    'collection',
    'name',
    'data',
  ])
  ->values([
    'collection' => '',
    'name' => 'swiffy_slider.settings',
    'data' => serialize([
      'configuration_url' => '',
    ]),
  ])
  ->execute();

$connection->insert('config')
  ->fields([
    'collection' => '',
    'name' => 'views.view.empty_configuration_url',
    'data' => serialize(Yaml::decode(file_get_contents(__DIR__ . '/views.view.empty_configuration_url.yml'))),
  ])
  ->execute();
// Replace core.entity_view_display.node.article.default.
$connection->update('config')
  ->fields([
    'data' => serialize(Yaml::decode(file_get_contents(__DIR__ . '/core.entity_view_display.node.article.default.yml'))),
  ])
  ->condition('collection', '')
  ->condition('name', 'core.entity_view_display.node.article.default')
  ->execute();
